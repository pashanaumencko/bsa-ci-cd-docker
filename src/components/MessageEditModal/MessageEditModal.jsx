import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';
import { editMessageAction, hidePageAction } from '../../actions/actions';

class MessageEditModal extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    const editedMessageText = event.target.elements.editMessage.value;
    const messageObj = this.props.message;
    messageObj.message = editedMessageText
    if(editedMessageText) {
      this.props.editMessageAction(messageObj);
      this.props.hidePageAction();
    }
  } 

  onHide(event) {
    this.props.hidePageAction();
  } 

  render() {
    return (
      <div className="modal" style={{ display: "block" }} tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Edit message</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onHide}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form id="editMessageForm" onSubmit={this.onSubmit}>
                <div className="form-group">
                  <label htmlFor="message-text" className="col-form-label">Message:</label>
                  <textarea className="form-control" name="editMessage" defaultValue={this.props.message.message} ></textarea>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="submit" form="editMessageForm" className="btn btn-primary">Send message</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    message: state.message,
  }
};

const mapDispatchToProps = {
  ...actions,
  editMessageAction, 
  hidePageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditModal);