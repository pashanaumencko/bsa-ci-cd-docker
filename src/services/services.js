import uuidv4 from 'uuid/v4';

const getUniqueId = () => uuidv4();

export { getUniqueId };