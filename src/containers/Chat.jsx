import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/actions';
import { getMessagesAction } from '../actions/actions';
import Header from '../components/Header/Header';
import moment from 'moment';
import MessageList from '../components/MessageList/MessageList';
import MessageInput from '../components/MessageInput/MessageInput';
import MessageEditModal from '../components/MessageEditModal/MessageEditModal';
import Spinner from '../components/Spinner/Spinner';


import './Chat.css';

class Сhat extends React.Component {
  componentDidMount() {
    const API_URL = 'https://api.myjson.com/bins/1hiqin.json';

    fetch(API_URL)
      .then(result => result.json())
      .then(json => this.props.getMessagesAction(json))
      .catch(error => console.log(error));
  }

  getSortedMessagesByDate() {
    // const timeFormat = 'YYYY-MM-DD HH:mm:ss';
    const messages = this.props.messages;
    return messages
      .sort((firstMessage, secondMessage) => moment.utc(firstMessage.timeStamp).diff(moment.utc(secondMessage.timeStamp)));
      // .reverse();
  }

  getHeader() {
    const messages = this.getSortedMessagesByDate();
    return <Header messages={messages} />;
  }

  getMessageList() {
    const messages = this.getSortedMessagesByDate();
    return <MessageList messages={messages} />;
  }

  getMessageInput() {
    return <MessageInput />
  }

  render() {
    return this.props.isLoaded
      ? (
        <div className="container">
          <div className="chat">
            {this.getHeader()}
            {this.getMessageList()}
            {this.getMessageInput()}
            {this.props.isShown ? <MessageEditModal /> : null}
          </div>
        </div>
        )
      : <Spinner />; 
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    messages: state.messages,
    isShown: state.isShown,
    isLoaded: state.isLoaded,
  };
};

const mapDispatchToProps = {
  ...actions,
  getMessagesAction, 
};


export default connect(mapStateToProps, mapDispatchToProps)(Сhat);
