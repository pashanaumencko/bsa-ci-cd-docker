export const GET_DATA = 'GET_DATA';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const SHOW_PAGE = 'SHOW_PAGE';
export const HIDE_PAGE = 'HIDE_PAGE';
export const SET_EDIT_MESSAGE = 'SET_EDIT_MESSAGE';