FROM node:8.9-alpine

WORKDIR /run

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
